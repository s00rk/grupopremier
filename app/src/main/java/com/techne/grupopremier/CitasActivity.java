package com.techne.grupopremier;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class CitasActivity extends ActionBarActivity {

    private String url = "http://www.newkontrol.mx/erp/ws/citas_app.php";
    private Handler progressBarHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citas);

        Util.GetMyCars();

        final ProgressDialog dialog = new ProgressDialog(CitasActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Cargando");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Util.ctx = CitasActivity.this;
        Util.ctxCitas = CitasActivity.this;

        final ListView lv1 = (ListView) findViewById(R.id.lista_citas);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                Cita newsData = (Cita) o;
                Intent i = new Intent(CitasActivity.this, CitaDetailsActivity.class);
                i.putExtra("Id", newsData.Id_cita);
                startActivity(i);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                GetCitas();
                progressBarHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        lv1.setAdapter(new ListViewCitas(CitasActivity.this, Util.Citas));
                        if(Util.Citas.size() == 0)
                            Util.crearDialogo("NO HAY CITAS", CitasActivity.this);
                    }
                });

                dialog.dismiss();
            }
        }).start();



        ImageButton btnback = (ImageButton)findViewById(R.id.citas_btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageButton btnAdd = (ImageButton)findViewById(R.id.citas_btnadd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CitasActivity.this, CitaAddActivity.class);
                startActivity(i);
            }
        });


    }

    private void GetCitas()
    {
        Util.Citas = new ArrayList<Cita>();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tipo", "get");
        String series = "";
        for(Auto a : Util.MisAutos)
            if(series.length() > 0)
                series += ",'" + a.Serie + "'";
            else
                series = "'" + a.Serie + "'";
        params.put("series", series);
        Log.e("Series", series);
        try {
            Object data = Util.SendData(url, params);

            if(data == null)
            {
                return;
            }
            JSONArray arrobj = (JSONArray)data;
            Cita or;
            Log.e("CITAS", arrobj.length()+"");
            for(int i = 0; i < arrobj.length(); i++)
            {

                or = new Cita();
                JSONObject obj = arrobj.getJSONObject(i);
                Log.e("CITAS", obj.toString()+"");
                or.Anio = obj.getString("anio");
                or.Id_cita = obj.getString("id_cita");
                or.Celular = obj.getString("celular");
                or.Color = obj.getString("color");
                or.Email = obj.getString("email");
                or.Fecha = obj.getString("fecha");
                or.Fecha_Solicitud = obj.getString("fecha_solicitud");
                or.Hora = obj.getString("hora");
                or.Hora_Solicitud = obj.getString("hora_solicitud");
                or.Marca = obj.getString("marca");
                or.Modelo = obj.getString("modelo");
                or.Orden_id = obj.getString("orden_id");
                or.Serie = obj.getString("serie");
                or.Status_cita = obj.getString("status_cita");
                or.Tipo_Cita = obj.getString("tipo_cita");


                Util.Citas.add(or);
            }
            Log.e("CITAS", Util.Citas.size()+"");
        }catch (Exception ex){
            Log.e("CITAS", ex.toString());
        }
    }


    private boolean Delete(String id_cita, String motivos_cancelacion, String observaciones)
    {
        try{
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("tipo", "cancela");
            params.put("id_cita", id_cita);
            params.put("motivos_cancelacion", motivos_cancelacion);
            params.put("observaciones", observaciones);

            Object data = Util.SendData(url, params);
            if(data == null)
            {
                return false;
            }
            JSONArray arrobj = (JSONArray)data;
            JSONObject jObj = arrobj.getJSONObject(0);
            if(jObj.getString("ok").equals("2"))
            {
                Toast.makeText(CitasActivity.this, jObj.getString("error"), Toast.LENGTH_LONG).show();
                return false;
            }
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return false;
    }
}
