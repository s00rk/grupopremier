package com.techne.grupopremier;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class OrderRepacionActivity extends ActionBarActivity {

    private String url = "http://www.newkontrol.mx/erp/ws/ordenes_app.php";
    private Handler progressBarHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_repacion);


        final ProgressDialog dialog = new ProgressDialog(OrderRepacionActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Cargando");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Util.ctx = OrderRepacionActivity.this;
        //GetOrders();

        final ListView lv1 = (ListView) findViewById(R.id.lista_orderrep);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                OrderRepacion newsData = (OrderRepacion) o;
                Intent i = new Intent(OrderRepacionActivity.this, OrderReapcionDetailsActivity.class);
                i.putExtra("Id", newsData.Id);
                startActivity(i);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                GetOrders();
                progressBarHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        lv1.setAdapter(new ListViewOrderRepacion(OrderRepacionActivity.this, Util.OrdersRepacion));
                        if(Util.OrdersRepacion.size() == 0)
                            Util.crearDialogo("NO HAY ORDENES", OrderRepacionActivity.this);
                    }
                });

                dialog.dismiss();
            }
        }).start();



        ImageButton btnback = (ImageButton)findViewById(R.id.orderrep_btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //dialog.dismiss();


    }

    private void GetOrders()
    {
        Util.OrdersRepacion = new ArrayList<OrderRepacion>();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tipo", "get");
        String series = "";
        for(Auto a : Util.MisAutos)
            if(series.length() > 0)
                series += ",'" + a.Serie + "'";
            else
                series = "'"+ a.Serie + "'";
        params.put("series", series);
        try {
            Object data = Util.SendData(url, params);
            if(data == null)
            {
                return;
            }
            JSONArray arrobj = (JSONArray)data;
            OrderRepacion or;
            for(int i = 0; i < arrobj.length(); i++)
            {
                or = new OrderRepacion();
                JSONObject obj = arrobj.getJSONObject(i);
                or.Status_orden = obj.getString("status_orden");
                or.Id = obj.getString("id");
                or.Hora_recepcion = obj.getString("hora_recepcion");
                or.Hora_entrega = obj.getString("hora_entrega");
                or.Fecha_recepcion = obj.getString("fecha_recepcion");
                or.Fecha_entrega = obj.getString("fecha_entrega");
                or.Tipo = obj.getString("tipo");
                or.Status_orden_id = obj.getString("status_orden_id");
                or.Apodo = obj.getString("apodo");
                or.Serie = obj.getString("serie");
                or.Marca = obj.getString("marca");
                or.Modelo = obj.getString("modelo");
                or.Anio = obj.getString("anio");
                or.Placa = obj.getString("placa");
                or.Kilometraje = obj.getString("kilometraje");
                or.Cliente_id = obj.getString("cliente_id");
                or.Trabajos_orden = obj.getString("trabajos_orden");
                or.Folio_orden = obj.getString("folio_orden");
                Util.OrdersRepacion.add(or);
            }
        }catch (Exception ex){

        }
    }


}
