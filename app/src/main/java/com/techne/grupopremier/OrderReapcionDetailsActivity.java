package com.techne.grupopremier;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class OrderReapcionDetailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_reapcion_details);

        String id = getIntent().getStringExtra("Id");
        OrderRepacion ord = null;
        for(OrderRepacion or : Util.OrdersRepacion)
            if(or.Id.equals(id))
                ord = or;

        if(ord == null)
            finish();
        TextView status, vehiculo, fecha_rec, hora_rec, fecha_ent, hora_ent, descripcion, servicio;
        ImageView Status_img = (ImageView)findViewById(R.id.orderdetail_image);

        status = (TextView)findViewById(R.id.orderdetail_status);
        status.setText(ord.Status_orden);
        Auto a = null;

        for(Auto aa : Util.MisAutos) {
            if (aa.Serie.equals(ord.Serie))
                a = aa;
        }
        if(a != null) {
            vehiculo = (TextView)findViewById(R.id.orderdetailreap_lblvehiculo);
            vehiculo.setText(a.Apodo);
        }


        servicio = (TextView)findViewById(R.id.orderdetail_service);
        servicio.setText(ord.Tipo);

        fecha_rec = (TextView)findViewById(R.id.orderdetail_fecha_rec);
        fecha_rec.setText(ord.Fecha_recepcion);
        hora_rec = (TextView)findViewById(R.id.orderdetail_hora_rec);
        hora_rec.setText(ord.Hora_recepcion);
        fecha_ent = (TextView)findViewById(R.id.orderdetail_fecha_ent);
        fecha_ent.setText(ord.Fecha_entrega);
        hora_ent = (TextView)findViewById(R.id.orderdetail_hora_ent);
        hora_ent.setText(ord.Hora_entrega);
        descripcion = (TextView)findViewById(R.id.orderdetail_descripcion);
        descripcion.setText(ord.Trabajos_orden);

        switch(ord.Status_orden.toLowerCase())
        {
            case "trabajando":
            case "citado":
                Status_img.setImageResource(R.drawable.status_cita_0);
                break;
            case "facturada":
                Status_img.setImageResource(R.drawable.status_cita_1);
                break;
            case "cancelada":
                Status_img.setImageResource(R.drawable.status_cita_4);
                break;
            case "unidad lista":
                Status_img.setImageResource(R.drawable.status_cita_3);
                break;
        }

        ImageButton btnback = (ImageButton)findViewById(R.id.orderdetail_btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
