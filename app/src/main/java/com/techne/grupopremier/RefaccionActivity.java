package com.techne.grupopremier;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class RefaccionActivity extends ActionBarActivity {
    private Handler progressBarHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refaccion);

        final ProgressDialog dialog = new ProgressDialog(RefaccionActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Cargando");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Util.ctx = RefaccionActivity.this;

        final ListView lv1 = (ListView) findViewById(R.id.lista_refaccion);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                Refaccion newsData = (Refaccion) o;
                Intent i = new Intent(RefaccionActivity.this, RefaccionDetailActivity.class);
                i.putExtra("Id", newsData.Id_pedido);
                startActivity(i);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                GetRefacciones();
                progressBarHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        lv1.setAdapter(new ListViewRefaccion(RefaccionActivity.this, Util.Refacciones));
                        if(Util.Refacciones.size() == 0)
                            Util.crearDialogo("NO HAY REFACCIONES", RefaccionActivity.this);
                    }
                });
                Log.e("App", Util.Refacciones.size()+"");
                dialog.dismiss();
            }
        }).start();

        ImageButton btnback = (ImageButton)findViewById(R.id.refaccion_btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void GetRefacciones(){
        Log.e("App", "Refacciones");
        Util.Refacciones = new ArrayList<Refaccion>();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tipo", "get");
        String series = "";
        for(Auto a : Util.MisAutos)
            if(series.length() > 0)
                series += ",'" + a.Serie + "'";
            else
                series = "'"+a.Serie+"'";
        params.put("series", series);
        try {
            Object data = Util.SendData("http://wwww.newkontrol.mx/erp/ws/pedidos_refacciones_app.php", params);
            if(data == null)
            {
                return;
            }
            JSONArray arrobj = (JSONArray)data;
            Refaccion or;
            Log.e("App", arrobj.length()+"");
            for(int i = 0; i < arrobj.length(); i++)
            {
                or = new Refaccion();
                JSONObject obj = arrobj.getJSONObject(i);
                or.Agencia_id = obj.getString("agencia_id");
                or.Anio = obj.getString("anio");
                or.Cantidad_entregada = obj.getString("cantidad_entregada");
                or.Cantidad_pedida = obj.getString("cantidad_pedida");
                or.Cantidad_quellego = obj.getString("cantidad_quellego");
                or.Fecha_entrega = obj.getString("fecha_entrega");
                or.Fecha_pedido = obj.getString("fecha_pedido");
                or.Fecha_surtido = obj.getString("fecha_surtido");
                or.Id_pedido = obj.getString("id_pedido");
                or.Serie = obj.getString("serie");
                or.Marca = obj.getString("marca");
                or.Modelo = obj.getString("modelo");
                or.Numero = obj.getString("numero");
                or.Refaccion_descripcion = obj.getString("refaccion_descipcion");
                or.Refaccion_status = obj.getString("refacion_status");
                Util.Refacciones.add(or);
                Log.e("App", (i+1)+"");
            }
        }catch (Exception ex){
            Log.e("App", ex.toString());
            ex.printStackTrace();
        }
    }


}
