package com.techne.grupopremier;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;


public class AutoDetailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_details);
        Auto auto = null;
        String id = getIntent().getStringExtra("Serie");
        for(Auto a : Util.MisAutos)
            if(a.Serie.equals(id))
                auto = a;
        if(auto == null)
            finish();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tipo", "get");
        params.put("serie", auto.Serie);
        try {
            JSONObject objArr = ((JSONArray) Util.SendData("http://www.newkontrol.mx/erp/ws/vehiculos_app.php", params)).getJSONObject(0);
            auto.Modelo = objArr.getString("modelo");
            auto.Color = objArr.getString("color");
            auto.Anio = objArr.getString("anio");
        }catch (Exception ex){
            finish();
        }
        ImageButton btnback = (ImageButton)findViewById(R.id.autodetail_btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView titulo, vehiculo, celular, email, marca, modelo, anio, color, serie;
        ImageView image = (ImageView)findViewById(R.id.orderdetail_image);

        vehiculo = (TextView)findViewById(R.id.autodetail_lblvehiculo);
        titulo = (TextView)findViewById(R.id.autodetail_lbltitle);
        celular = (TextView)findViewById(R.id.autodetail_cel);
        serie = (TextView)findViewById(R.id.autodetail_serie);
        email = (TextView)findViewById(R.id.autodetail_email);
        marca = (TextView)findViewById(R.id.autodetail_marca);
        modelo = (TextView)findViewById(R.id.autodetail_modelo);
        anio = (TextView)findViewById(R.id.autodetail_anio);
        color = (TextView)findViewById(R.id.autodetail_color);

        vehiculo.setText(auto.Apodo);
        titulo.setText(auto.Apodo);
        celular.setText(auto.Celular);
        serie.setText(auto.Serie);
        email.setText(auto.Email);
        marca.setText(auto.Marca);
        modelo.setText(auto.Modelo);
        anio.setText(auto.Anio);
        color.setText(auto.Color);

        int idres = R.drawable.toyota;

        // Agregar lo de marcas
        switch (auto.Marca.toLowerCase())
        {
            case "bmw":
                idres = R.drawable.bmw;
                break;
            case "chevrolet":
                idres = R.drawable.chevrolet;
                break;
            case "chrysler":
                idres = R.drawable.chrysler;
                break;
            case "dodge":
                idres = R.drawable.dodge;
                break;
            case "fiat":
                idres = R.drawable.fiat;
                break;
            case "gmc":
                idres = R.drawable.gmc;
                break;
            case "hino":
                idres = R.drawable.hino;
                break;
            case "hyundai":
                idres = R.drawable.hyundai;
                break;
            case "jaguar":
                idres = R.drawable.jaguar;
                break;
            case "jeep":
                idres = R.drawable.jeep;
                break;
            case "land rover":
                idres = R.drawable.land_rover;
                break;
            case "mercedes benz":
                idres = R.drawable.mercedes_benz;
                break;
            case "mini":
                idres = R.drawable.mini;
                break;
            case "ram":
                idres = R.drawable.ram;
                break;
            case "smart":
                idres = R.drawable.smart;
                break;
            case "suzuki":
                idres = R.drawable.suzuki;
                break;
        }
        image.setImageResource(idres);

    }
}
