package com.techne.grupopremier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewOrderRepacion extends BaseAdapter {

    private ArrayList<OrderRepacion> listData;
    private LayoutInflater layoutInflater;

    private static class Menuholder
    {
        TextView Fecha;
        TextView Hora;
        TextView Vehiculo;
        TextView Status;
        TextView Titulo;
        ImageView Status_img;
    }

    public ListViewOrderRepacion(Context aContext, ArrayList<OrderRepacion> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Menuholder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_orderrep, null);
            holder = new Menuholder();
            holder.Fecha = (TextView) convertView.findViewById(R.id.orderrep_fecha);
            holder.Hora = (TextView) convertView.findViewById(R.id.orderrep_hora);
            holder.Vehiculo = (TextView) convertView.findViewById(R.id.refaccion_vehiculo);
            holder.Status = (TextView) convertView.findViewById(R.id.orderrep_status);
            holder.Titulo = (TextView) convertView.findViewById(R.id.orderrep_title);
            holder.Status_img = (ImageView) convertView.findViewById(R.id.refaccion_icono);
            convertView.setTag(holder);
        } else {
            holder = (Menuholder) convertView.getTag();
        }

        holder.Fecha.setText(listData.get(position).Fecha_recepcion);
        holder.Hora.setText(listData.get(position).Hora_recepcion);
        holder.Titulo.setText(listData.get(position).Tipo);
        holder.Status.setText(listData.get(position).Status_orden);

        Auto a = null;
        for(Auto aa : Util.MisAutos) {
            if (aa.Serie.equals(listData.get(position).Serie))
                a = aa;
        }
        if(a != null)
            holder.Vehiculo.setText(a.Apodo);


        switch(listData.get(position).Status_orden.toLowerCase())
        {
            case "trabajando":
            case "citado":
                holder.Status_img.setImageResource(R.drawable.status_cita_0);
                break;
            case "facturada":
                holder.Status_img.setImageResource(R.drawable.status_cita_1);
                break;
            case "cancelada":
                holder.Status_img.setImageResource(R.drawable.status_cita_4);
                break;
            case "unidad lista":
                holder.Status_img.setImageResource(R.drawable.status_cita_3);
                break;
        }
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
