package com.techne.grupopremier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewCitas extends BaseAdapter {

    private ArrayList<Cita> listData;
    private LayoutInflater layoutInflater;

    private static class Menuholder
    {
        TextView Fecha;
        TextView Hora;
        TextView Vehiculo;
        TextView Status;
        TextView Titulo;
        ImageView Status_img;
    }

    public ListViewCitas(Context aContext, ArrayList<Cita> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Menuholder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_citas, null);
            holder = new Menuholder();
            holder.Fecha = (TextView) convertView.findViewById(R.id.citas_fecha);
            holder.Hora = (TextView) convertView.findViewById(R.id.citas_hora);
            holder.Vehiculo = (TextView) convertView.findViewById(R.id.citas_vehiculo);
            holder.Status = (TextView) convertView.findViewById(R.id.citas_status);
            holder.Titulo = (TextView) convertView.findViewById(R.id.citas_title);
            holder.Status_img = (ImageView) convertView.findViewById(R.id.citas_icono);
            convertView.setTag(holder);
        } else {
            holder = (Menuholder) convertView.getTag();
        }

        holder.Fecha.setText(listData.get(position).Fecha_Solicitud);

        String hora = listData.get(position).Hora;
        String hora_solicitud = listData.get(position).Hora_Solicitud;
        if(!hora.equals("00:00:00"))
            holder.Hora.setText(hora);
        else
            if(hora_solicitud.equals("1"))
                holder.Hora.setText("MAÑANA");
            else
                holder.Hora.setText("TARDE");

        holder.Titulo.setText(listData.get(position).Tipo_Cita);
        holder.Status.setText(listData.get(position).Status_cita);

        Auto a = null;
        for(Auto aa : Util.MisAutos) {
            if (aa.Serie.equals(listData.get(position).Serie))
                a = aa;
        }
        if(a != null)
            holder.Vehiculo.setText(a.Apodo);


        switch(listData.get(position).Status_cita.toLowerCase())
        {
            case "solicitada":
            case "citado":
                holder.Status_img.setImageResource(R.drawable.status_cita_0);
                 break;
            case "confirmada":
            case "aceptada":
                holder.Status_img.setImageResource(R.drawable.status_cita_1);
                break;
            case "cancelada":
                holder.Status_img.setImageResource(R.drawable.status_cita_4);
                break;
            case "unidad lista":
                holder.Status_img.setImageResource(R.drawable.status_cita_3);
                break;
        }
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
