package com.techne.grupopremier;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;


public class MyCarAddActivity extends ActionBarActivity {

    private String url = "http://www.newkontrol.mx/erp/ws/vehiculos_app.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_car_add);
        Util.ctx = MyCarAddActivity.this;


        ImageButton btnback = (ImageButton)findViewById(R.id.mycaradd_btnback);
        Button btnadd = (Button)findViewById(R.id.mycaradd_btnadd);
        Button btnqr = (Button)findViewById(R.id.mycaradd_btnqr);
        final EditText txtserie = (EditText)findViewById(R.id.mycaradd_txtserie);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("App", "inicia camara");
                IntentIntegrator.initiateScan(MyCarAddActivity.this);
            }
        });

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serie = txtserie.getText().toString();
                if(serie.length() == 0) {
                    Toast.makeText(MyCarAddActivity.this, "No puedes dejar la serie en blanco.", Toast.LENGTH_LONG).show();
                    return;
                }
                AgregarAuto(serie.toString());
            }
        });
    }

    private boolean mShowDialog = false;

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Log.i("App", "Llego resultado");
            if (resultCode == RESULT_OK) {

                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Log.i("App", contents);
                Log.i("App", format);
                final EditText txtserie = (EditText)findViewById(R.id.mycaradd_txtserie);
                txtserie.setText(contents);
                AgregarAuto(contents);


            } else if (resultCode == RESULT_CANCELED) {
                Log.i("App", "Scan unsuccessful");
            }

    }

    private void AgregarAuto(String serie)
    {
        boolean ok = false;
        for(Auto a : Util.MisAutos)
            if(a.Serie.equals(serie)) {
                Toast.makeText(MyCarAddActivity.this, "Este auto ya lo tienes registrado", Toast.LENGTH_LONG).show();
                return;
            }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tipo", "get");
        params.put("serie", serie);
        String marca = "";
        try {
            Object data = Util.SendData(url, params);
            if(data == null)
            {
                Toast.makeText(MyCarAddActivity.this, "Serie no identificada, porfavor revisar los datos introducidos!", Toast.LENGTH_LONG).show();
                return;
            }
            JSONObject obj = ((JSONArray)data).getJSONObject(0);

            if (obj.getString("ok").equals("1") == false) {
                Toast.makeText(MyCarAddActivity.this, obj.getString("error"), Toast.LENGTH_LONG).show();
                return;
            }
            marca = obj.getString("marca");
            ok = true;
        }catch (Exception ex){

        }

        if(!ok)
            return;


        final DialogFragmentAddCar ddialog = new DialogFragmentAddCar();
        ddialog.serie = serie;
        ddialog.marca = marca;
        ddialog.show(getSupportFragmentManager(), "add_car");
    }



}
