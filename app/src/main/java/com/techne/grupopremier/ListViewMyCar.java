package com.techne.grupopremier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewMyCar extends BaseAdapter {
    private ArrayList<Auto> listData;
    private LayoutInflater layoutInflater;

    public ListViewMyCar(Context aContext, ArrayList<Auto> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Menuholder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_mycar, null);
            holder = new Menuholder();
            holder.apodo = (TextView) convertView.findViewById(R.id.apodo_mycar);
            holder.serie = (TextView) convertView.findViewById(R.id.serie_mycar);
            holder.imagen = (ImageView)convertView.findViewById(R.id.refaccion_icono);


            convertView.setTag(holder);
        } else {
            holder = (Menuholder) convertView.getTag();
        }

        holder.apodo.setText(listData.get(position).Apodo);
        holder.serie.setText(listData.get(position).Serie);
        int idres = R.drawable.toyota;

        // Agregar lo de marcas
        switch (listData.get(position).Marca.toLowerCase())
        {
            case "bmw":
                idres = R.drawable.bmw;
                break;
            case "chevrolet":
                idres = R.drawable.chevrolet;
                break;
            case "chrysler":
                idres = R.drawable.chrysler;
                break;
            case "dodge":
                idres = R.drawable.dodge;
                break;
            case "fiat":
                idres = R.drawable.fiat;
                break;
            case "gmc":
                idres = R.drawable.gmc;
                break;
            case "hino":
                idres = R.drawable.hino;
                break;
            case "hyundai":
                idres = R.drawable.hyundai;
                break;
            case "jaguar":
                idres = R.drawable.jaguar;
                break;
            case "jeep":
                idres = R.drawable.jeep;
                break;
            case "land rover":
                idres = R.drawable.land_rover;
                break;
            case "mercedes benz":
                idres = R.drawable.mercedes_benz;
                break;
            case "mini":
                idres = R.drawable.mini;
                break;
            case "ram":
                idres = R.drawable.ram;
                break;
            case "smart":
                idres = R.drawable.smart;
                break;
            case "suzuki":
                idres = R.drawable.suzuki;
                break;
        }
        holder.imagen.setImageResource(idres);

        return convertView;
    }

    private static class Menuholder
    {
        TextView apodo;
        TextView serie;
        ImageView imagen;
    }

}