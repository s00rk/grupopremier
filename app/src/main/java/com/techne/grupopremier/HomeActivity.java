package com.techne.grupopremier;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class HomeActivity extends ActionBarActivity {

    ListView lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Util.ctx = HomeActivity.this;
        Util.GetMyCars();

        lista = (ListView)findViewById(R.id.lista_menu);

        ArrayList lista_menu = CreateList();

        final ListView lv1 = (ListView) findViewById(R.id.lista_menu);
        lv1.setAdapter(new ListViewMenu(this, lista_menu));
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                MENUITEM newsData = (MENUITEM) o;
                Intent i;
                switch(newsData.getTitulo().toLowerCase())
                {
                    case "mis autos":
                    i = new Intent(HomeActivity.this, MyCarActivity.class);
                    startActivity(i);
                    break;
                    case "ordenes de repacion":
                        if(Util.MisAutos.size() == 0) {
                            Toast.makeText(HomeActivity.this, "No tienes ningun auto registrado", Toast.LENGTH_LONG).show();
                            return;
                        }
                        i = new Intent(HomeActivity.this, OrderRepacionActivity.class);
                        startActivity(i);
                        break;
                    case "busca tu agencia":
                        i = new Intent(HomeActivity.this, AgenciasActivity.class);
                        startActivity(i);
                        break;
                    case "refacciones":
                        i = new Intent(HomeActivity.this, RefaccionActivity.class);
                        startActivity(i);
                        break;
                    case "solicitud citas":
                        i = new Intent(HomeActivity.this, CitasActivity.class);
                        startActivity(i);
                        break;
                    case "buzon de sugerencias":
                        i = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:soporte@grupo-premier.com.mx"));
                        i.putExtra(Intent.EXTRA_SUBJECT, "Contacto App Android");
                        startActivity(i);
                        break;

                }

            }
        });
    }


    private ArrayList<MENUITEM> CreateList()
    {
        ArrayList<MENUITEM> menu = new ArrayList<MENUITEM>();
        menu.add(new MENUITEM("Solicitud Citas", R.drawable.icono1));
        menu.add(new MENUITEM("Ordenes de Repacion", R.drawable.icono2));
        menu.add(new MENUITEM("Refacciones", R.drawable.icono3));
        menu.add(new MENUITEM("Busca Tu Agencia", R.drawable.icono4));
        menu.add(new MENUITEM("Mis Autos", R.drawable.icono6));
        menu.add(new MENUITEM("Proximo Servicio", R.drawable.icono8));
        menu.add(new MENUITEM("Buzon de Sugerencias", R.drawable.icono9));

        return menu;
    }


}
