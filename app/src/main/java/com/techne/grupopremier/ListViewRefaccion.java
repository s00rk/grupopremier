package com.techne.grupopremier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewRefaccion extends BaseAdapter {

    private ArrayList<Refaccion> listData;
    private LayoutInflater layoutInflater;

    private static class Menuholder
    {
        TextView Fecha;
        TextView Cantidad;
        TextView Status;
        ImageView Status_img;
    }

    public ListViewRefaccion(Context aContext, ArrayList<Refaccion> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Menuholder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_refaccion, null);
            holder = new Menuholder();
            holder.Fecha = (TextView) convertView.findViewById(R.id.refaccion_fecha);
            holder.Cantidad = (TextView) convertView.findViewById(R.id.refaccion_cantidad);
            holder.Status = (TextView) convertView.findViewById(R.id.refaccion_status);
            holder.Status_img = (ImageView) convertView.findViewById(R.id.refaccion_icono);
            convertView.setTag(holder);
        } else {
            holder = (Menuholder) convertView.getTag();
        }

        holder.Fecha.setText(listData.get(position).Fecha_pedido);
        holder.Cantidad.setText(listData.get(position).Cantidad_pedida);
        holder.Status.setText(listData.get(position).Refaccion_status);



        switch(listData.get(position).Refaccion_status.toLowerCase())
        {
            case "solicitada":
                holder.Status_img.setImageResource(R.drawable.status_cita_0);
                break;
            case "recibida":
                holder.Status_img.setImageResource(R.drawable.status_cita_1);
                break;
            case "cancelada":
                holder.Status_img.setImageResource(R.drawable.status_cita_4);
                break;
            case "en camino":
                holder.Status_img.setImageResource(R.drawable.status_cita_3);
                break;
        }
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
