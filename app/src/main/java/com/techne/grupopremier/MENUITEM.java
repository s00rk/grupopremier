package com.techne.grupopremier;

/**
 * Created by s00rk on 19/03/15.
 */
public class MENUITEM {

    private String titulo;
    private int imagen;

    public MENUITEM(){}
    public MENUITEM(String t, int i){
        titulo = t;
        imagen = i;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}
