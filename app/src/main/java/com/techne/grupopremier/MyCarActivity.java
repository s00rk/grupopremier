package com.techne.grupopremier;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyCarActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_car);
        Util.ctx = MyCarActivity.this;
        Util.ctxMyCar = MyCarActivity.this;
        Util.GetMyCars();

        final ListView lv1 = (ListView) findViewById(R.id.lista_mycar);
        final ListViewMyCar adapter = new ListViewMyCar(this, Util.MisAutos);
        lv1.setAdapter(adapter);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                Auto newsData = (Auto) o;
                Intent i = new Intent(MyCarActivity.this, AutoDetailsActivity.class);
                i.putExtra("Serie", newsData.Serie);
                startActivity(i);
            }
        });
        lv1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                final Auto newsData = (Auto) o;

                AlertDialog.Builder builder = new AlertDialog.Builder(MyCarActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Seguro Que Desea Eliminar?");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // How to remove the selected item?
                        JSONArray jArr = new JSONArray();
                        for(Auto a : Util.MisAutos) {
                            if (!a.Serie.equals(newsData.Serie)) {
                                try {
                                    JSONObject nobj = new JSONObject();
                                    nobj.put("serie", a.Serie);
                                    nobj.put("celular", a.Celular);
                                    nobj.put("email", a.Email);
                                    nobj.put("apodo", a.Apodo);
                                    nobj.put("marca", a.Marca);
                                    jArr.put(nobj);

                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }
                            }
                        }
                        SharedPreferences sharedPref = Util.ctx.getSharedPreferences("appData", Context.MODE_PRIVATE);
                        String json = sharedPref.getString("jsonData", "");
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("jsonData", jArr.toString());
                        editor.commit();
                        dialog.dismiss();
                        ((Activity)Util.ctxMyCar).finish();
                    }

                });

                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                    }

                });

                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
        });

        ImageButton btnback = (ImageButton)findViewById(R.id.mycar_btnback);
        ImageButton btnadd = (ImageButton)findViewById(R.id.mycar_btnadd);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyCarActivity.this, MyCarAddActivity.class);
                startActivity(i);
            }
        });
    }



}
