package com.techne.grupopremier;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by s00rk on 19/03/15.
 */
public class DialogFragmentAddCar extends android.support.v4.app.DialogFragment {
    EditText apodo, email, celular;
    String serie, marca;

    @Override
    public void onDestroy() {
        super.onDestroy();
        Util.destroyMyCarAdd();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View viewRoot = inflater.inflate(R.layout.dialog_add_new_car, null);


        builder.setView(viewRoot)
                // Add action buttons
                .setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        celular = (EditText)viewRoot.findViewById(R.id.addcar_txtcelular);
                        email = (EditText)viewRoot.findViewById(R.id.addcar_txtemail);
                        apodo = (EditText)viewRoot.findViewById(R.id.addcar_txtapodo);
                        if (apodo.getText().toString().length() == 0 || celular.getText().toString().length() == 0 || email.getText().toString().length() == 0) {
                            Util.Mensaje("No debes dejar campos vacios");
                            return;
                        }
                        try {

                            SharedPreferences sharedPref = Util.ctx.getSharedPreferences("appData", Context.MODE_PRIVATE);
                            String json = sharedPref.getString("jsonData", "");
                            JSONObject nobj = new JSONObject();
                            nobj.put("serie", serie);
                            nobj.put("celular", celular.getText().toString());
                            nobj.put("email", email.getText().toString());
                            nobj.put("apodo", apodo.getText().toString());
                            nobj.put("marca", marca);
                            JSONArray jArr = new JSONArray();

                            if (json.length() > 2) {
                                jArr = Util.ParseJson(json);
                                jArr.put(nobj);
                            } else {
                                jArr.put(nobj);
                            }

                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString("jsonData", jArr.toString());
                            editor.commit();
                            Util.result = true;
                            Util.Mensaje("Auto Agregado!");
                            return;
                        }catch (Exception ex){
                            Util.Mensaje(ex.getMessage());
                        }
                        Util.Mensaje("Error al intentar guardar el auto.");
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Util.result = false;
                        DialogFragmentAddCar.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
