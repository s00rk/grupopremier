package com.techne.grupopremier;

/**
 * Created by s00rk on 22/03/15.
 */
public class OrderRepacion {

    public String Id;
    public String Hora_recepcion;
    public String Hora_entrega;
    public String Fecha_recepcion;
    public String Fecha_entrega;
    public String Tipo;
    public String Folio_orden;
    public String Status_orden;
    public String Status_orden_id;
    public String Apodo;
    public String Serie;
    public String Marca;
    public String Modelo;
    public String Anio;
    public String Placa;
    public String Kilometraje;
    public String Cliente_id;
    public String Trabajos_orden;

    public OrderRepacion(){}
}
