package com.techne.grupopremier;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by s00rk on 19/03/15.
 */
public class Util {

    static ArrayList<Auto> MisAutos = new ArrayList<Auto>();
    static ArrayList<OrderRepacion> OrdersRepacion = new ArrayList<OrderRepacion>();
    static ArrayList<Refaccion> Refacciones = new ArrayList<Refaccion>();
    static ArrayList<Cita> Citas = new ArrayList<Cita>();

    static Context ctx, ctxMyCar, ctxCitas;
    static boolean result;

    public static void crearDialogo(String mensaje, Context ctx)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(mensaje)
                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false);
        builder.create().show();
    }

    public static void GetMyCars()
    {
        Util.MisAutos = new ArrayList<Auto>();
        SharedPreferences sharedPref = ctx.getSharedPreferences("appData", Context.MODE_PRIVATE);
        String json = sharedPref.getString("jsonData", "");
        if(json.length() <= 0)
            return;

        try {
            JSONArray jArr = Util.ParseJson(json);

            for(int i=0; i < jArr.length(); i++)
            {
                JSONObject obj = jArr.getJSONObject(i);
                Auto a = new Auto();
                a.Apodo = obj.getString("apodo");
                a.Serie = obj.getString("serie");
                a.Email =  obj.getString("email");
                a.Celular = obj.getString("celular");
                a.Marca = obj.getString("marca");


                Util.MisAutos.add(a);

            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public static void destroyMyCarAdd()
    {
        if(result == true) {
            ((Activity) ctx).finish();
            ((Activity) ctxMyCar).finish();
        }
    }

    public static JSONArray ParseJson(String data)
    {
        try {
            JSONArray jObj = new JSONArray(data);
            JSONObject obj = jObj.getJSONObject(0);
            return jObj;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public static Object SendData(String WS, HashMap<String, String> params)
    {
        try {
            URL obj = new URL(WS);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            String urlParameters = "";

            for(Map.Entry<String, String> cur : params.entrySet())
            {
                if(urlParameters.length() > 0)
                    urlParameters += "&";
                urlParameters += cur.getKey() + "=" + cur.getValue();
            }

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            if(response.toString().equals("null") || response.toString().length() == 0)
                return null;
            return Util.ParseJson(response.toString());
        }catch(Exception ex){
            ex.printStackTrace();
            Mensaje(ex.getMessage());
        }
        return null;

    }

    public static void Mensaje(Context ctx, String msj)
    {
        Toast.makeText(ctx, msj, Toast.LENGTH_LONG).show();
    }
    public static void Mensaje(String msj)
    {
        Toast.makeText(Util.ctx, msj, Toast.LENGTH_LONG).show();
    }

}
