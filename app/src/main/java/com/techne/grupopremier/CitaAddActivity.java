package com.techne.grupopremier;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CitaAddActivity extends ActionBarActivity {

    private String url = "http://www.newkontrol.mx/erp/ws/citas_app.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cita_add);

        ImageButton btnBack = (ImageButton)findViewById(R.id.citaadd_btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Switch hora = (Switch)findViewById(R.id.citaadd_hora);
        final Spinner autos = (Spinner)findViewById(R.id.citaadd_auto);
        final Spinner servicios = (Spinner)findViewById(R.id.citaadd_servicio);
        final Spinner agencias = (Spinner)findViewById(R.id.citaadd_agencia);
        Button enviar = (Button)findViewById(R.id.citaadd_btnEnviar);
        final EditText fecha = (EditText)findViewById(R.id.citaadd_fecha);
        final EditText email = (EditText)findViewById(R.id.citaadd_email);
        final EditText celular = (EditText)findViewById(R.id.citaadd_celular);

        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.fecha = fecha;
                newFragment.show(getSupportFragmentManager(), "datePicker");
                celular.requestFocus();
            }
        });
        fecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    DatePickerFragment newFragment = new DatePickerFragment();
                    newFragment.fecha = fecha;
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                    celular.requestFocus();
                }
            }
        });

        autos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Auto a = null;
                for(Auto aa : Util.MisAutos)
                    if(aa.Apodo.equals(autos.getSelectedItem().toString()))
                        a = aa;
                if(a != null)
                {
                    celular.setText(a.Celular);
                    email.setText(a.Email);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auto a = null;
                for(Auto aa : Util.MisAutos)
                    if(aa.Apodo.equals(autos.getSelectedItem().toString()))
                        a = aa;
                if(a == null)
                {
                    Toast.makeText(CitaAddActivity.this, "No se encontro el auto", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(fecha.getText().toString().equals(""))
                {
                    Toast.makeText(CitaAddActivity.this, "No se pueden dejar campos en blanco", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(hora.getText().toString().equals(""))
                {
                    Toast.makeText(CitaAddActivity.this, "No se pueden dejar campos en blanco", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(celular.getText().toString().equals(""))
                {
                    Toast.makeText(CitaAddActivity.this, "No se pueden dejar campos en blanco", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(email.getText().toString().equals(""))
                {
                    Toast.makeText(CitaAddActivity.this, "No se pueden dejar campos en blanco", Toast.LENGTH_SHORT).show();
                    return;
                }
                String hora_s = "2";
                if(hora.getText().toString().equals("Mañana"))
                    hora_s = "1";
                boolean resp = Insert(a.Serie, fecha.getText().toString(), hora_s , servicios.getSelectedItem().toString(), agencias.getSelectedItemPosition(),
                        celular.getText().toString(), email.getText().toString());
                if(resp) {
                    ((Activity)Util.ctxCitas).finish();
                    finish();
                }
            }
        });

        hora.setText("Tarde");
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String hora_s = hora.getText().toString();
                if(hora_s.equals("Mañana"))
                    hora.setText("Tarde");
                else
                    hora.setText("Mañana");
            }
        });

        List<String> list = new ArrayList<>();
        for(Auto a : Util.MisAutos)
            list.add(a.Apodo);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        autos.setAdapter(dataAdapter);

        List<String> lista = new ArrayList<>();
        lista.add("SERVICIO EXPRESS");
        lista.add("SERVICIO CORRECTIVO");
        lista.add("SERVICIO DE 10,000 KMS");
        lista.add("SERVICIO DE 20,000 KMS");
        lista.add("SERVICIO DE 30,000 KMS");
        lista.add("SERVICIO DE 40,000 KMS");
        lista.add("SERVICIO DE 50,000 KMS");
        lista.add("SERVICIO DE 60,000 KMS");
        lista.add("SERVICIO DE 70,000 KMS");
        lista.add("SERVICIO DE 80,000 KMS");
        lista.add("SERVICIO DE 90,000 KMS");
        lista.add("SERVICIO DE 100,000 KMS");
        ArrayAdapter<String> dataAdapters = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lista);
        dataAdapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicios.setAdapter(dataAdapters);


        List<String> listas = new ArrayList<>();
        listas.add("Toyota Culiacan");
        listas.add("Land Rover - Jaguar");
        listas.add("Hyundai Premier");
        listas.add("Premier Chevrolet Culiacan");
        listas.add("Chevrolet Mazatlan");
        listas.add("Premier Chevrolet Guamuchil");
        listas.add("Super Autos Premier Hermosillo");
        listas.add("GMC Culiacan");
        listas.add("GMC Los Mochis");
        listas.add("AutoCountry Culiacan");
        listas.add("AutoCountry Los Mochis");
        listas.add("Toyota Mazatlan");
        listas.add("Toyota Los Mochis");
        listas.add("Hyundau Premier TJ");
        listas.add("Susuki");
        listas.add("BMW AutoWelt Culiacan");
        listas.add("BMW Premier Valle Monterrey");
        listas.add("BMW Premier Saltillo");
        listas.add("BMW Motorrad Culiacan");
        listas.add("BMW Motorrad Monterrey");
        listas.add("BMW Motorrad Saltillo");
        listas.add("Mini Culiacan");
        listas.add("Mini Monterrey");
        listas.add("Mini Saltillo");
        listas.add("Fiat");
        listas.add("Hino Culiacan");
        listas.add("Mercedes Benz");
        ArrayAdapter<String> dataAdapterss = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listas);
        dataAdapterss.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        agencias.setAdapter(dataAdapterss);

        if(Util.MisAutos.size() > 0) {
            email.setText(Util.MisAutos.get(0).Email);
            celular.setText(Util.MisAutos.get(0).Celular);
        }
        celular.requestFocus();
    }

    private boolean Insert(String serie, String fecha_solicitud, String hora_solicitud, String tipo_cita, int agencia_id, String telefono, String email)
    {
        try{
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("tipo", "insert");
            params.put("serie", serie);
            params.put("fecha_solicitud", fecha_solicitud);
            params.put("hora_solicitud", hora_solicitud);
            params.put("tipo_cita", tipo_cita);
            params.put("agencia_id", agencia_id+"");
            params.put("telefono", telefono);
            params.put("email", email);

            Object data = Util.SendData(url, params);
            if(data == null)
            {
                return false;
            }
            JSONArray arrobj = (JSONArray)data;
            JSONObject jObj = arrobj.getJSONObject(0);
            if(jObj.getString("ok").equals("2"))
            {
                Toast.makeText(CitaAddActivity.this, jObj.getString("error"), Toast.LENGTH_LONG).show();
                return false;
            }
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

}
