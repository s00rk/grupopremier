package com.techne.grupopremier;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class RefaccionDetailActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refaccion_detail);
        String id = getIntent().getStringExtra("Id");
        
        Refaccion ref = null;
        for(Refaccion or : Util.Refacciones)
            if(or.Id_pedido.equals(id))
                ref = or;

        if(ref == null)
            finish();

        TextView status, fecha_ped, fecha_ent, cant_ped, cant_ent, descripcion, vehiculo;
        ImageView Status_img = (ImageView)findViewById(R.id.refaccdetail_image);

        status = (TextView)findViewById(R.id.refaccdetail_status);
        status.setText(ref.Refaccion_status);

        fecha_ped = (TextView)findViewById(R.id.refdetail_fecha);
        fecha_ped.setText(ref.Fecha_pedido);

        fecha_ent = (TextView)findViewById(R.id.refaccdetail_fecha_ent);
        fecha_ent.setText(ref.Fecha_entrega);
        cant_ped = (TextView)findViewById(R.id.refaccdetail_cant_ped);
        cant_ped.setText(ref.Cantidad_pedida);
        cant_ent = (TextView)findViewById(R.id.refaccdetail_cant_ent);
        cant_ent.setText(ref.Cantidad_entregada);
        descripcion = (TextView)findViewById(R.id.refaccdetail_descripcion);
        descripcion.setText(ref.Refaccion_descripcion);

        vehiculo = (TextView)findViewById(R.id.refdetail_vehiculo);

        Auto a = null;
        for(Auto aa : Util.MisAutos) {
            if (aa.Serie.equals(ref.Serie))
                a = aa;
        }
        if(a != null) {
            Log.e("App", a.Apodo);
            vehiculo.setText(a.Apodo);
        }

        switch(ref.Refaccion_status.toLowerCase())
        {
            case "solicitada":
                Status_img.setImageResource(R.drawable.status_cita_0);
                break;
            case "recibida":
                Status_img.setImageResource(R.drawable.status_cita_1);
                break;
            case "cancelada":
                Status_img.setImageResource(R.drawable.status_cita_4);
                break;
            case "en camino":
                Status_img.setImageResource(R.drawable.status_cita_3);
                break;
        }

        ImageButton btnback = (ImageButton)findViewById(R.id.refaccdetail_btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }



}
