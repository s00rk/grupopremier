package com.techne.grupopremier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewMenu extends BaseAdapter {
    private ArrayList<MENUITEM> listData;
    private LayoutInflater layoutInflater;

    public ListViewMenu(Context aContext, ArrayList<MENUITEM> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Menuholder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_menu, null);
            holder = new Menuholder();
            holder.titulo = (TextView) convertView.findViewById(R.id.titulo_menu);
            holder.imagen = (ImageView) convertView.findViewById(R.id.refaccion_icono);
            convertView.setTag(holder);
        } else {
            holder = (Menuholder) convertView.getTag();
        }

        holder.titulo.setText(listData.get(position).getTitulo());
        holder.imagen.setImageResource(listData.get(position).getImagen());
        //holder.imagen.setImageResource(R.drawable.icono1);
        return convertView;
    }

    private static class Menuholder
    {
        TextView titulo;
        ImageView imagen;
    }

}